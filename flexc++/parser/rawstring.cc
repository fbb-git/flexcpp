#include "parser.ih"

Parser::TextType Parser::rawString()
{
    d_rawStringContent = d_scanner.rawStringContent();
    return TextType::RAWSTRING;
}
