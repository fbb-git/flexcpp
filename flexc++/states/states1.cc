#include "states.ih"

// FBB: two initial states to avoid (unconfirmed) index-0 collisions when the
// FBB: first rule is a repetition, like a{2}

States::States()
:
    d_state(2)
{}
