#include "states.ih"

#include <ostream>
#include <iomanip>

ostream &operator<<(ostream &out, States const &states)
{
    out <<
        " nr: type -> next1, next2 (0: final, next2 != 0 for '|' rules)\n"
        " type: E: EMPTY, F: FINAL, S: SET, idx: range idx\n"
        " NR  TYPE N1 N2 [range-indices]\n"
        "--------------------------------------------------------------\n";
    for (size_t idx = 0, end = states.size(); idx != end; ++idx)
    {
        if (states[idx].type() != States::UNDETERMINED_)
            out << setw(3) << idx << ": " << states[idx] << '\n';
    }
    return out;
}
