#include "generator.ih"

void Generator::baseIncludes(ostream &out) const
{
    bool interactive = d_options.interactive();
    bool caseSensitive = d_options.caseSensitive();
    string const &pre = d_options.baseclassPreinclude();

    if (interactive or not caseSensitive or not pre.empty())
        key(out);

    if (interactive)
        out << "#include <sstream>\n";

    if (not caseSensitive)
        out << "#include <cctype>\n";

    if (not pre.empty())
    {
        bool abs = pre.front() == '<';
       
        out << "#include " << 
               (abs ? "" : "\"") << pre << (abs ? "\n" : "\"\n");
    }

    if (!d_debug)
        return;

    if (!interactive)
    {
        key(out);
        out << "#include <sstream>\n";
    }

    out <<  "#include <set>\n"
            "#include <iomanip>\n";
}
