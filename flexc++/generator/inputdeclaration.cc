#include "generator.ih"

void Generator::inputDeclaration(std::ostream &out) const
{
    key(out);
                                        // no user-provided input-interface
                                        // d_input is private, otherwise: 
                                        // d_input is protected
    if (d_options.inputInterface().empty()) 
        out << R"(
    Input           *d_input;               // input now in d_streamStack

protected:
)";
    else
        out << R"(
protected:
    Input           *d_input;               // input now in d_streamStack
)";

}







