#include "fpattern.ih"

FPattern FPattern::alternatives(States &states, 
                                    FPattern const &lhs, 
                                    FPattern const &rhs)
{
    Options::regexCall("alternatives");

    Pair pair = states.next2();
    states[pair.first] = State(EMPTY, lhs.begin(), rhs.begin());

    states[lhs.end()] = State(EMPTY, pair.second, 0);
    states[rhs.end()] = State(EMPTY, pair.second, 0);

    FPattern ret(pair);

    if 
    (
        lhs.fixedLength() 
        && rhs.fixedLength() 
        && lhs.d_length == rhs.d_length
    )
        ret.d_length = lhs.d_length;

    return ret;
}



