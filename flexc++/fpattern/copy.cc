// #define XERR
#include "fpattern.ih"

// Data:   fpattern: the initial fpattern
//         begin, end: the begin/end state indices of the initial fpattern
//         beginEnd: vector of pairs holding begin and end indices of copied
//                   fpatterns
//
//         lower and upper are the lower and upper limits of an Interval
// 
// First the initial number of required fpatterns is duplicated (the 1st one 
//         already being available as it is `fpattern' itself)
// 
// If the upper repetition limit is not set, make the last fpattern repeatable,
//         (and join the fpatterns) and leave.
// 
// Otherwise: dup the remaining fpatterns, allocating an extra state to allow 
//         for exits in between the fpattern boundaries. 
// 
// Once the remaining fpatterns are dupped, add their jumps to the end-state
// 
// Finally join all the fpatterns and return the begin-end states of the final
// (merged) fpattern.

FPattern FPattern::copy(States &states, 
                      FPattern &regex, size_t lower, size_t upper)
{
    PairVector beginEnd(1, Pair{regex.begin(), regex.end()});   
                                                        // begin/end indices
                                                        // of the regex

    copyFPattern(states, lower - 1, beginEnd);           // copy req'd fpatterns

    return
        upper == max<size_t>() ?    // no upper limit: 
            optRepeatLastFPattern(states,            // optionally  repeat the 
                            regex, lower, beginEnd) // last fpattern. 
        :                                       // Otherwise add fixed nr
            optionalFPatterns(states, regex,     // of optional fpatterns
                            lower, upper, beginEnd);
}


