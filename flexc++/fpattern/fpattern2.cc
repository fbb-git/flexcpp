#include "fpattern.ih"

FPattern::FPattern(States &states, FPattern const &lopLhs, FPattern const &lopRhs, 
                 size_t startCondition)
:
    d_lopData(new LopData {startCondition, lopLhs.pair().second,
                           lopLhs.duplicate(states) }),    
    d_pair(concatenate(states, lopLhs, lopRhs).pair())
{}
