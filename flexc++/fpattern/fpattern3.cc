#include "fpattern.ih"

// This is a fixed-sized tail.

FPattern::FPattern(States &states, size_t tailLength,
                    FPattern const &lopLhs, FPattern const &lopRhs)
:
    d_lopData(new LopData {tailLength, 0} ),    
    d_pair(concatenate(states, lopLhs, lopRhs).pair())
{}
