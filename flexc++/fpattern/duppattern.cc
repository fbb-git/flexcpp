//#define XERR
#include "fpattern.ih"

// Map: key: a state idx, value: the state to transit to

// a State 
//        d_type is the state's RANGES index (or F: final state etc.).  
//        d_rule is the rule associated with the state
//        its StateData:   
//            next1: the state to transit to, at 0: no next state
//            next2: if next1 == 0: not used, otherwise

size_t FPattern::dupFPattern(Map &map, States &states, size_t idx)
{
            // done if the idx is already in `map'
    auto iter = map.find(idx);
    if (iter != map.end())
        return iter->second;

        // add a new state
    size_t newIdx = states.next();

    map[idx] = newIdx;                   // define the transition

        // inspect the current state's transitions
    StateData const &link = states[idx].data();

    states[newIdx].dup(
                        states[idx], 
                        dupFPattern(map, states, link.next1()),
                        dupFPattern(map, states, link.next2())
                    );

    return newIdx;
}



