//#define XERR
#include "fpattern.ih"

FPattern FPattern::optionalFPatterns(States &states, FPattern &fpattern,
                                        size_t lower, size_t upper, 
                                        PairVector &beginEnd)
{
        // dup the remaining fpatterns, allowing for intermediate ejects
    Map eject;
    copyFPattern(states, lower, upper, eject, beginEnd);

        // add jumps to the end of the fpattern dupped last
    jumpToEnd(states, beginEnd, lower, upper, eject);

        // join all fpatterns, the first fpattern being `fpattern' (=semval)
    join(states, fpattern, upper, beginEnd);

        // begin/end must be updated to the final indices
    FPattern ret( {fpattern.begin(), fpattern.end()} );

    return ret;
}





