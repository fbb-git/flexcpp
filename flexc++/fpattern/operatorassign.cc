#include "fpattern.ih"

FPattern &FPattern::operator=(Pair const &pair)
{
    d_pair = pair;
    d_lopData.reset();
    return *this;
}
