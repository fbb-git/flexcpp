#include "fpattern.ih"

FPattern FPattern::dot(States &states)
{
    CharClass charClass;
    charClass.append(vector<string>(1, CharClass::predefined(".")));

    FPattern ret = characterClass(states, charClass);

    ret.d_length = 1;
    return ret;
}
