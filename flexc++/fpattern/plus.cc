#include "fpattern.ih"

FPattern FPattern::plus(States &states, FPattern const &fpattern)
{
    Pair pair = states.next2();      // create new Start/Final states

        // fpattern's end connects to fpattern begin and the new FINAL state.
    states[fpattern.end()] = State(EMPTY, fpattern.begin(), pair.second);

        // Start state connects to the fpattern's begin
    states[pair.first] = State(EMPTY, fpattern.begin(), 0);
    
    FPattern ret(pair);

    return ret;
}



