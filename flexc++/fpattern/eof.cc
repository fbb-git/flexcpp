#include "fpattern.ih"

FPattern FPattern::eof(States &states)
{
    Pair pair = states.next2();

    states[pair.first] = State(EOF_, pair.second, 0);

    FPattern ret(pair);
    return ret;
}
